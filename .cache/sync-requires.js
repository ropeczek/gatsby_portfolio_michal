const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---cache-caches-gatsby-plugin-offline-app-shell-js": hot(preferDefault(require("/home/marcin/portfolio/gatsby_portfolio_michal/.cache/caches/gatsby-plugin-offline/app-shell.js"))),
  "component---src-pages-404-jsx": hot(preferDefault(require("/home/marcin/portfolio/gatsby_portfolio_michal/src/pages/404.jsx"))),
  "component---src-pages-animals-jsx": hot(preferDefault(require("/home/marcin/portfolio/gatsby_portfolio_michal/src/pages/animals.jsx"))),
  "component---src-pages-casma-jsx": hot(preferDefault(require("/home/marcin/portfolio/gatsby_portfolio_michal/src/pages/casma.jsx"))),
  "component---src-pages-gyard-jsx": hot(preferDefault(require("/home/marcin/portfolio/gatsby_portfolio_michal/src/pages/gyard.jsx"))),
  "component---src-pages-just-jsx": hot(preferDefault(require("/home/marcin/portfolio/gatsby_portfolio_michal/src/pages/just.jsx"))),
  "component---src-pages-optimum-jsx": hot(preferDefault(require("/home/marcin/portfolio/gatsby_portfolio_michal/src/pages/optimum.jsx"))),
  "component---src-pages-papix-jsx": hot(preferDefault(require("/home/marcin/portfolio/gatsby_portfolio_michal/src/pages/papix.jsx"))),
  "component---src-pages-villagecourier-jsx": hot(preferDefault(require("/home/marcin/portfolio/gatsby_portfolio_michal/src/pages/villagecourier.jsx"))),
  "component---src-pages-virus-jsx": hot(preferDefault(require("/home/marcin/portfolio/gatsby_portfolio_michal/src/pages/virus.jsx"))),
  "component---src-templates-top-index-jsx": hot(preferDefault(require("/home/marcin/portfolio/gatsby_portfolio_michal/src/templates/top-index.jsx")))
}

