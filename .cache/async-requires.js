// prefer default export if available
const preferDefault = m => (m && m.default) || m

exports.components = {
  "component---cache-caches-gatsby-plugin-offline-app-shell-js": () => import("./../../caches/gatsby-plugin-offline/app-shell.js" /* webpackChunkName: "component---cache-caches-gatsby-plugin-offline-app-shell-js" */),
  "component---src-pages-404-jsx": () => import("./../../../src/pages/404.jsx" /* webpackChunkName: "component---src-pages-404-jsx" */),
  "component---src-pages-animals-jsx": () => import("./../../../src/pages/animals.jsx" /* webpackChunkName: "component---src-pages-animals-jsx" */),
  "component---src-pages-casma-jsx": () => import("./../../../src/pages/casma.jsx" /* webpackChunkName: "component---src-pages-casma-jsx" */),
  "component---src-pages-gyard-jsx": () => import("./../../../src/pages/gyard.jsx" /* webpackChunkName: "component---src-pages-gyard-jsx" */),
  "component---src-pages-just-jsx": () => import("./../../../src/pages/just.jsx" /* webpackChunkName: "component---src-pages-just-jsx" */),
  "component---src-pages-optimum-jsx": () => import("./../../../src/pages/optimum.jsx" /* webpackChunkName: "component---src-pages-optimum-jsx" */),
  "component---src-pages-papix-jsx": () => import("./../../../src/pages/papix.jsx" /* webpackChunkName: "component---src-pages-papix-jsx" */),
  "component---src-pages-villagecourier-jsx": () => import("./../../../src/pages/villagecourier.jsx" /* webpackChunkName: "component---src-pages-villagecourier-jsx" */),
  "component---src-pages-virus-jsx": () => import("./../../../src/pages/virus.jsx" /* webpackChunkName: "component---src-pages-virus-jsx" */),
  "component---src-templates-top-index-jsx": () => import("./../../../src/templates/top-index.jsx" /* webpackChunkName: "component---src-templates-top-index-jsx" */)
}

