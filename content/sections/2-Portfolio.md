---
anchor: "Portfolio"
header: "Portfolio"
subheader: "„W projektowaniu liczą się przede wszystkim emocje i uczucia, a nie sama technologia.”"
portfolios: [
  {
    imageFileName: "portfolio_home/casma.jpg",
    # imageFileNameDetail: "portfolio_home/01-full.jpg",
    header: "Casma",
    subheader: "Illustration",
    Title: "casma",
    # content: "Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!",
    extraInfo: ["Date: January 2017", "Client: Casma", "Category: Illustration"]
  },
  {
    imageFileName: "portfolio_home/gyard.png",
    # imageFileNameDetail: "portfolio_home/02-full.jpg",
    header: "Gyard",
    subheader: "Graphic Design",
    Title: "gyard",
    # content: "Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!",
    extraInfo: ["Date: January 2017", "Client: Explorer", "Category: Graphic Design"]
  },
  {
    imageFileName: "portfolio_home/optimum.jpg",
    # imageFileNameDetail: "portfolio_home/04-full.jpg",
    header: "Optimum",
    subheader: "Branding",
    Title: "optimum",
    # content: "Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!",
    extraInfo: ["Date: January 2017", "Client: Lines", "Category: Branding"]
  },
  {
    imageFileName: "portfolio_home/papix.jpg",
    # imageFileNameDetail: "portfolio_home/05-full.jpg",
    header: "Papix",
    subheader: "Website Design",
    Title: "papix",
    # content: "Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!",
    extraInfo: ["Date: January 2017", "Client: Southwest", "Category: Website Design"]
  },
  {
    imageFileName: "portfolio_home/villageCourier.jpg",
    # imageFileNameDetail: "portfolio_home/06-full.jpg",
    header: "Village Courier",
    subheader: "Photography",
    Title: "villagecourier",
    # content: "Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!",
    extraInfo: ["Date: January 2017", "Client: Window", "Category: Photography"]
  },
  {
    imageFileName: "portfolio_home/virus.jpg",
    # imageFileNameDetail: "portfolio_home/06-full.jpg",
    header: "Virus",
    subheader: "Graphic",
    Title: "virus",
    # content: "Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!",
    extraInfo: ["Date: January 2017", "Client: Window", "Category: Photography"]
  },
  {
    imageFileName: "portfolio_home/just.jpg",
    # imageFileNameDetail: "portfolio_home/06-full.jpg",
    header: "Just",
    subheader: "Illustration",
    Title: "just",
    # content: "Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!",
    extraInfo: ["Date: January 2017", "Client: Window", "Category: Photography"]
  },
  {
    imageFileName: "portfolio_home/animals.jpg",
    # imageFileNameDetail: "portfolio_home/06-full.jpg",
    header: "Animals",
    subheader: "Graphic",
    Title: "aniamals",
    # content: "Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!",
    extraInfo: ["Date: January 2017", "Client: Window", "Category: Photography"]
  }
]
---
