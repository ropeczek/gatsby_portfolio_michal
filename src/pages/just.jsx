import React from "react";
import PhotoGallery from "../components/PhotoGallery";
import { Just as photos } from 'components/GalleryPhotos/Just';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'gatsby';
import "./global.scss";


const Just = () => (
  <>
  <div className="container">
    <div className="row">
        <div className="col-12">
          <PhotoGallery photos={photos()} title="Just" className="xd" />
        </div>
    </div>
    <div className="row">
      <div className="col-12 back d-flex justify-content-center"><Link to="/#Portfolio" className="link d-flex justify-content-center"><FontAwesomeIcon icon={faArrowLeft} size="3x" /><span className="align-self-center ml-2">Wróć</span></Link></div>
    </div>
  </div>
  </>
);

export default Just;