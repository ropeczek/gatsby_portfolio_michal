import React from "react";
import PhotoGallery from "../components/PhotoGallery";
import { Gyard as photos } from 'components/GalleryPhotos/Gyard';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'gatsby';
import "./global.scss";


const Gyard = () => (
  <>
  <div className="container">
    <div className="row">
        <div className="col-12">
          <PhotoGallery photos={photos()} title="Gyard" className="xd" />
        </div>
    </div>
    <div className="row">
      <div className="col-12 back d-flex justify-content-center"><Link to="/#Portfolio" className="link d-flex justify-content-center"><FontAwesomeIcon icon={faArrowLeft} size="3x" /><span className="align-self-center ml-2">Wróć</span></Link></div>
    </div>
  </div>
  </>
);

export default Gyard;