import React from 'react';
import Navbar from '../views/Navbar/Navbar';
import Footer from "views/Footer";
import { useStaticQuery, graphql } from "gatsby";

const frontmatterNavbar = {
    brand: 'PEARLANT',
    menuText: 'Menu ',
};

const anchors = ['Oferta', 'Portfolio', 'O mnie', 'Kontakt'];
const frontmatter = {
    "copyright": "Copyright © By Anonymous 2020",
    "social": {
        "twitter": "#",
        "facebook": "#",
        "medium": "#",
        "behance": "#",
      }
}

export const query = graphql`
    {
        markdownRemark {
            frontmatter {
                copyright
                privacyHref
                privacyText
                termsHref
                termsText
                social {
                    facebook
                    github
                    linkedin
                    medium
                    twitter
                    behance
                }
            }
        }
    }
`;

const MainLayout = ({children, location}) => {
    return (
        <>
            <Navbar
                anchors={anchors}
                frontmatter={frontmatterNavbar}
                disabledShrink={location.pathname != '/' ? true : false}
                location={location}
            />
            {children}

            <Footer frontmatter={frontmatter} />
        </>
    )
};

export default MainLayout;