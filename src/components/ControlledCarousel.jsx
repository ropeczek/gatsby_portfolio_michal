import React, { useState } from "react";
import PropTypes from "prop-types";
import { useStaticQuery, graphql } from "gatsby";
import Image from 'gatsby-image';
import Carousel from 'react-bootstrap/Carousel';

import "./ControlledCarousel.scss";

const ControlledCarousel = () => {
    const [index, setIndex] = useState(0);
    const data = useStaticQuery(query);
    // const {allFile: {nodes}} = data;
    const {allMdx: {nodes}} = data;
    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
    };

    return (
        <Carousel className="carousel" activeIndex={index} onSelect={handleSelect}>
            {
                nodes.map((node, index) => {
                    const {frontmatter: {featuredImage: {childImageSharp: {fluid}}}} = node;
                    const {frontmatter: {title, more}} = node;
                    return (
                        <Carousel.Item key={index}>
                            <Image
                                className="d-block w-100 image"
                                fluid={fluid}
                                alt={title}
                                key={title}
                            />
                            <Carousel.Caption>
                                <h3>{title}</h3>
                                <p>{more}</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                    )
                })
            }
        </Carousel>
    );
};

export const query = graphql`
    {
        allFile(filter: {relativeDirectory: {regex: "/(slider)/"}}) {
            nodes {
                childImageSharp {
                    fluid(maxWidth: 1900, quality: 100) {
                        ...GatsbyImageSharpFluid_tracedSVG
                        originalName
                    }
                }
            }
        }
        allMdx {
            nodes {
              frontmatter {
                title
                more
                featuredImage {
                  childImageSharp {
                    fluid(maxWidth: 1900, quality: 100) {
                      ...GatsbyImageSharpFluid_tracedSVG
                    }
                  }
                }
              }
            }
        }
    }
`;


ControlledCarousel.propTypes = {
    data: PropTypes.any,
};

ControlledCarousel.defaultProps = {
    data: PropTypes.any,
};

export default ControlledCarousel;