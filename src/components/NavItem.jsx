import React from "react";
import PropTypes from "prop-types";
import { Nav } from "react-bootstrap";
import { Link } from 'react-scroll';

import "./NavItem.scss";


const NavItem = ({ to, onClick, children, href, location }) => {
  console.log(location);
  if (href == '') href = to;
  return (
    <Nav.Item>
      {
        location.pathname == '/' ?
          (
            <Link
              className="nav-link cursor-pointer"
              activeClass="active"
              to={to}
              spy
              smooth="easeInOutQuart"
              onClick={onClick}
            >
              {children || to}
            </Link>
        ) : (
          <a href={`/#${to}`} className="nav-link cursor-pointer">{to}</a>
        )
      }
    </Nav.Item>
  );
};

NavItem.propTypes = {
  to: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.any,
  href: PropTypes.any
};

NavItem.defaultProps = {
  to: "",
  onClick: null,
  children: null,
  href: '',
};

export default NavItem;
