import React from "react";
import PropTypes from "prop-types";
import { Link } from 'gatsby';

import { Col } from "react-bootstrap";
import Image from "components/Image";
import Icon from "components/Icon";
import PortfolioDetailDialog from "components/PortfolioDetailDialog";

import "./PortfolioItem.scss";

const PortfolioItem = ({
  imageFileName,
  imageAlt,
  header,
  subheader,
  to,
  // content,
  imageFileNameDetail,
  imageAltDetail,
  extraInfo,
}) => {
console.log(imageAlt);
  return (
    <>
      <Col md={4} sm={6} className="portfolio-item">
        <Link to={to}
          role="button"
          tabIndex={-1}
          className="portfolio-link"
          data-toggle="modal"
        >
          <Image
            className="img-fluid img"
            fileName={imageFileName}
            alt={imageAlt || header || subheader}
          />
        </Link>
        <div className="portfolio-caption">
          <h4>{header}</h4>
          {subheader ? <p className="text-muted">{subheader}</p> : null}
        </div>
      </Col>
    </>
  );
};

PortfolioItem.propTypes = {
  imageFileName: PropTypes.string.isRequired,
  imageAlt: PropTypes.string,
  header: PropTypes.string.isRequired,
  subheader: PropTypes.string,
  // content: PropTypes.string,
  imageFileNameDetail: PropTypes.string,
  imageAltDetail: PropTypes.string,
  extraInfo: PropTypes.any,
};

PortfolioItem.defaultProps = {
  imageAlt: "",
  subheader: "",
  // content: "",
  imageFileNameDetail: "",
  imageAltDetail: "",
  extraInfo: null,
};

export default PortfolioItem;
