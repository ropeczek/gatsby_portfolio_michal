import React from "react";
import PropTypes from "prop-types";

import CircleIcon from "components/CircleIcon";

const Behance = ({ userName }) => (
  <CircleIcon href={`https://Behance.net/${userName}`} iconName="BehanceIcon" />
);

Behance.propTypes = {
  userName: PropTypes.string.isRequired,
};

export default Behance;
