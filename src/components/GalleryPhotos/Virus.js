import React from 'react';
import { useStaticQuery, graphql } from "gatsby";

export const query = graphql`
  {
    allFile(filter: {relativeDirectory: {regex: "/(virus)/"}}) {
      nodes {
          childImageSharp {
              fluid(maxWidth: 1900, quality: 100) {
                  ...GatsbyImageSharpFluid_tracedSVG
                  originalName
              }
          }
      }
    }
  }
`;

export const Virus = () => {
  const data = useStaticQuery(query);
  const {allFile: {nodes}} = data;

  return (
  nodes.map((node) => {
    const {childImageSharp: {fluid: tracedSVG}} = node;
    console.log(tracedSVG);
    return (
      {
        src: tracedSVG.src,
        width: 3,
        height: 3,
      }
    )
  })
  );
};